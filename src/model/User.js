export default class User {
  constructor(user) {
    this.id = user.login.uuid;
    this.gender = user.gender.substr(0, 1).toUpperCase();
    this.name = `${user.name.title} ${user.name.first} ${user.name.last}`;
    this.email = user.email;
    this.nationality = user.nat;
    this.age = user.dob.age.toString();
    this.birthDate = user.dob.date.substr(0, 10);
    this.registeredDate = user.registered.date.substr(0, 10);
    this.email = user.email;
    this.city = user.location.city;
    this.street = user.location.street.name;
    this.location = {
      latitude: user.location.coordinates.latitude,
      longitude: user.location.coordinates.longitude,
    };
    this.phone = user.phone;
    this.cell = user.cell;
    this.imageProfile = user.picture.large;
  }
}
