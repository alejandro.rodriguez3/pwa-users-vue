import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import User from '../model/User';

Vue.use(Vuex);

const MAX_USERS = 15;

export default new Vuex.Store({
  state: {
    users: [],
    favourites: [],
    filtersApplied: {
      gender: '',
      age: '',
      nationality: '',
    },
  },
  mutations: {
    addUser(state, user) {
      state.users.push(new User(user));
    },
    addUserToFavourites(state, user) {
      const userToAdd = state.users.find((userData) => userData.id === user.id);
      const index = state.favourites.findIndex((favUser) => favUser.id === user.id);
      if (index >= 0) {
        state.favourites.splice(index, 1);
      } else {
        state.favourites.push(userToAdd);
      }
    },
    removeFilters(state) {
      state.filtersApplied.gender = '';
      state.filtersApplied.age = '';
      state.filtersApplied.nationality = '';
    },
  },
  actions: {
    getUserDataList({ commit }) {
      for (let i = 1; i <= MAX_USERS; i += 1) {
        axios.get('https://randomuser.me/api/')
          .then((response) => {
            commit('addUser', response.data.results[0]);
          })
          .catch((error) => {
            throw error;
          });
      }
    },
    addUserToFavourites({ commit }, user) {
      commit('addUserToFavourites', user);
    },
    removeFilters({ commit }) {
      commit('removeFilters');
    },
  },
  getters: {
    userDataList: (state) => state.users,
    filteredUserDataList: (state) => Object.entries(state.filtersApplied).reduce(
      (users, [field, value]) => {
        if (!value) return users;
        return users.filter((user) => value === user[field]);
      }, state.users,
    ),
    favouriteUserDataList: (state) => state.favourites,
    genderOptions: (state) => [...new Set(state.users.map((user) => user.gender))],
    nationalityOptions: (state) => [...new Set(state.users.map((user) => user.nationality))].sort(),
    isFavouriteUser: (state) => (userId) => state.favourites.findIndex(
      (user) => user.id === userId,
    ) >= 0,
  },
});
