import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import UserInfo from '../views/UserInfo.vue';
import FavouriteUsers from '../views/FavouriteUsers.vue';

import PageNotFound from '../views/PageNotFound.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/user',
    name: 'UserInfo',
    component: UserInfo,
  },
  {
    path: '/favourites',
    name: 'Favourites',
    component: FavouriteUsers,
  },
  {
    path: '**',
    name: 'PageNotFound',
    component: PageNotFound,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
